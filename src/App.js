import React, { Component } from 'react';
import './App.css';
import Header from './components/layout/Header';
import Main from './Main'

class App extends Component {
  render() {
    return (
      <div className="App container">
        <Header />
        <Main />
      </div>
    );
  }
}

export default App;
