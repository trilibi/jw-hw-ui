import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './components/Home'
import Equipment from './components/catalog/Equipment'

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/equipment' component={Equipment}/>
    </Switch>
  </main>
)

export default Main
