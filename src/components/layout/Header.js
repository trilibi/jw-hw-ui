import React from 'react'
import logo from '../../img/logo.png';
import { Link } from 'react-router-dom'

const Header = () => (
  <nav className="navbar navbar-default navbar-fixed-top">
    <div className="container">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a className="navbar-brand" href="/">
          <img src={logo} className="App-logo" alt="logo" />
        </a>
      </div>
      <div id="navbar" className="navbar-collapse collapse">
        <ul className="nav navbar-nav">
          <li>
            <Link className="nav-item nav-link" to='/'>Home</Link>
          </li>
          <li>
            <Link className="nav-item nav-link" to='/equipment'>Equipment</Link>
          </li>
          <li>
            <Link className="nav-item nav-link" to='/equipment/admin'>Equipment Admin</Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>
)

export default Header
