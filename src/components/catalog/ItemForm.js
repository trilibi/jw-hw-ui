import React from 'react'
import EquipmentAPI from '../../Equipment'
import { Link } from 'react-router-dom'
import './item.css';

class ItemForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      model: null,
      item: {
          hours: 0,
          miles: 0,
          vin: '',
          serial: '',
          value: 0,
          notes: '',
          images: {
            values: []
          }
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
      EquipmentAPI.get({
          user: this.props.match.params.user,
          date: this.props.match.params.date
        }, (err, data) => {
          if (err) {
              console.warn(err);
          } else {
            let original = this.state.item;
            let item = Object.assign(original, data.simple);
            this.setState({
                model: data.model,
                item: item
            });
          }
      });
  }

  handleChange(event) {
    const target = event.target;
    const item = this.state.item;
    item[target.name] = target.value;
    this.setState({item: item});
  }

  handleSubmit(event) {
    EquipmentAPI.save({
        model: this.state.model,
        simple: this.state.item
    }, (err, data) => {
      if (err) {
        console.warn(err);
      } else {
          this.props.history.push('/equipment/admin');
      }
    });
    event.preventDefault();
  }

  render () {

    let item = this.state.item;

    if (!item) {
      return <div>Sorry, but the item was not found!</div>
    }

    return (
      <div className="item">
        <h2>{item.description} (#{item.id})</h2>

        <form className="form-horizontal" onSubmit={this.handleSubmit}>

          <div className="form-group">
            <label htmlFor="hours" className="col-sm-2 control-label">Hours</label>
            <div className="col-sm-2">
              <input className="form-control" type="text" id="hours" name="hours" value={item.hours} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="miles" className="col-sm-2 control-label">Miles</label>
            <div className="col-sm-2">
              <input className="form-control" type="text" id="miles" name="miles" value={item.miles} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="vin" className="col-sm-2 control-label">VIN</label>
            <div className="col-sm-2">
              <input className="form-control" type="text" id="vin" name="vin" value={item.vin} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="serial" className="col-sm-2 control-label">Serial</label>
            <div className="col-sm-2">
              <input className="form-control" type="text" id="serial" name="serial" value={item.serial} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="value" className="col-sm-2 control-label">Value</label>
            <div className="col-sm-2">
              <input className="form-control" type="text" id="value" name="value" value={item.value} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="notes" className="col-sm-2 control-label">Notes</label>
            <div className="col-sm-4">
              <textarea className="form-control" id="notes" name="notes" value={item.notes} onChange={this.handleChange}/>
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="submit" className="btn btn-primary">Finish Editing</button>
            </div>
          </div>
        </form>

        <Link to='/equipment/admin'>Back</Link>
      </div>
    )
  }
}

export default ItemForm
