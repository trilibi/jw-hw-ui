import React from 'react'
import EquipmentAPI from '../../Equipment'
import { Link } from 'react-router-dom'
import Attributes from './_attributes'
import './item.css';

class Item extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            model: null,
            item: {
                hours: 0,
                miles: 0,
                vin: '',
                serial: '',
                value: 0,
                notes: '',
                images: {
                    values: []
                }
            }
        };
    }

    componentDidMount() {
        EquipmentAPI.get({
            user: this.props.match.params.user,
            date: this.props.match.params.date
        }, (err, data) => {
            if (err) {
                console.warn(err);
            } else {
                let original = this.state.item;
                let item = Object.assign(original, data.simple);
                this.setState({
                    model: data.model,
                    item: item
                });
            }
        });
    }

    render () {

        let item = this.state.item;
        let model = this.state.model;

        if (!model) {
            return <div>Sorry, but the item was not found!</div>
        }

        return (
            <div className="item">
                <h2>{model.description.S} (#{model.gen_id.S})</h2>

                <Attributes item={item}/>

                <Link to='/equipment'>Back</Link>
            </div>
        );
    }
}

export default Item
