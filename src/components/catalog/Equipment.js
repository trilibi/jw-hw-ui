import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Index from './Index'
import Item from './Item'
import ItemForm from './ItemForm'

const Equipment = () => (
  <div>
    <h1>Lee Hillery Equipment Catalog</h1>
    <Switch>
      <Route exact path='/equipment' component={Index}/>
      <Route exact path='/equipment/admin' render={()=><Index admin={true}/>}/>
      <Route path='/equipment/admin/:user/:date' component={ItemForm}/>
      <Route path='/equipment/:user/:date' component={Item}/>
    </Switch>
  </div>
)

export default Equipment
