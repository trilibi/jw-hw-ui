import React from 'react'
import EquipmentAPI from '../../Equipment'
import { Link } from 'react-router-dom'
import Attributes from './_attributes'

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          items: []
        };
    }

    componentDidMount() {
      EquipmentAPI.all({}, (err, data) => {
        if (err) {
            console.warn(err);
        } else {
          this.setState({items: data.Items});
        }
      });
    }

    render () {
      const admin = this.props.admin;
      return (
        <div>
          {
            this.state.items.map(item => (
              <div key={item.gen_id} className="item">
                <h2>{item.description}
                  {admin
                      ? <Link className="btn btn-default btn-xs" to={`/equipment/admin/${item.user}/${item.create_date}`}>Edit</Link>
                      : <Link className="btn btn-default btn-xs" to={`/equipment/${item.user}/${item.create_date}`}>View</Link>
                  }</h2>
                <Attributes item={item} />
              </div>
            ))
          }
        </div>
      );
    }
}

export default Index
