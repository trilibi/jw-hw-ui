import React from 'react'

export default (props) => {
    const item = props.item;

    if (!item) {
        return (<span>No Attributes</span>)
    }

    let images = item.images && item.images.values ? item.images.values : [];

  return (
    <div>
      <dl className="dl-horizontal">
        <dt>Hours</dt>
        <dd>{item.hours}</dd>
        <dt>Miles</dt>
        <dd>{item.miles}</dd>
        <dt>VIN</dt>
        <dd>{item.vin}</dd>
        <dt>Serial</dt>
        <dd>{item.serial}</dd>
        <dt>Value</dt>
        <dd>{item.value}</dd>
        <dt>Notes</dt>
        <dd>{item.notes}</dd>
      </dl>

      <div>
        {images.map((img, k) => <img className="item-thumbnail" key={k} src={img} alt="" />)}
      </div>
    </div>
  )
}
