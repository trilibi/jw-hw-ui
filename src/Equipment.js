// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.
import AWS from 'aws-sdk';

AWS.config.update({
    region: "us-east-1",
    //endpoint: 'http://localhost:8000',
    // accessKeyId default can be used while using the downloadable version of DynamoDB.
    // For security reasons, do not store AWS Credentials in your files. Use Amazon Cognito instead.
    accessKeyId: "AKIAJMYF4DUWO7NU2CAA",
    // secretAccessKey default can be used while using the downloadable version of DynamoDB.
    // For security reasons, do not store AWS Credentials in your files. Use Amazon Cognito instead.
    secretAccessKey: "4QrW7pwmfH5YhenjywXqpl+53OV+knNa5q0Vw/AU"
});

const db =  new AWS.DynamoDB(
    "https://dynamodb.us-east-1.amazonaws.com/", {}
);
const client = new AWS.DynamoDB.DocumentClient();
const tableName = 'cststarlordpwhwstg-mobilehub-1805522713-equipment';

const fields = {
  value: 'N'
};

function convertValueByFieldType(fieldType, value) {
    if (fieldType === 'SS') {
      value = value && value.hasOwnProperty('values') ? value.values : value;
      value = value ? value : [];
    }
    if (fieldType === 'S') {
        // cant set strings to blank, so one space
        value = value !== '' ? value : ' ';
    }
    if (fieldType === 'N') {
        // set blank to 0 string
        value = parseInt(value, 10) > 0 ? value.toString() : '0';
    }
    return value;
}

const EquipmentAPI = {
  equipment: [
    {
      'id': 1,
      'user': 'jasonwoolliscroft@gmail.com',
      'create_date': '2018-01-07T15:00:54.944-0600',
      'description': 'Home Made Shear For Skid Loader',
      'gen_id': '000001EF',
      'hours': 0,
      'images': ['https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGjwW4SpiGj0kzDvNxC0WO98GCAD6P5mkklmuzInf4mK5HalYuMw'],
      'location': 'Winona',
      'make': 'unknown',
      'miles': 0,
      'model': 'unknown',
      'notes': 'Reserved for notes',
      'owner': 'Jason',
      'personal_id': 'unknown',
      'serial': 'N/A',
      'sticker_id': 'G2424',
      'user_email': 'jasonwoolliscroft@gmail.com',
      'vin': 'N/A',
      'year': 'unknown',
      'value': 9000
    },
    {
      'id': 2,
      'user': 'jasonwoolliscroft@gmail.com',
      'create_date': '2018-01-07T15:00:54.944-0600',
      'description': '2014 Case IH 6140 Combine',
      'gen_id': '000001EF',
      'hours': 348,
      'images': ['https://assets.machinerypete.com/uploads/image/processed_image/3818477/full_size_img.axd', 'https://assets.machinerypete.com/uploads/image/processed_image/3175600/full_size_img.axd'],
      'location': 'Winona',
      'make': 'unknown',
      'miles': 0,
      'model': 'unknown',
      'notes': '198 threshing hours',
      'owner': 'Jason',
      'personal_id': 'unknown',
      'serial': 'YEG02161',
      'sticker_id': '',
      'user_email': 'jasonwoolliscroft@gmail.com',
      'vin': 'N/A',
      'year': 'unknown',
      'value': 98000
    },
    {
      'id': 3,
      'user': 'jasonwoolliscroft@gmail.com',
      'create_date': '2018-01-07T15:00:54.944-0600',
      'description': '2015 Volvo G746N Motor Grader',
      'gen_id': '000001EF',
      'hours': 7500,
      'images': ['http://www.canadianundergroundinfrastructure.com/files/slides/locale_image/medium/0066/16427_en_b4867_1763_volvo-g946c-motor-grader.jpg', 'https://sc02.alicdn.com/kf/HTB1.uuyJVXXXXaIXFXXq6xXFXXXX/used-Volvo-990-Motor-Grader.jpg', 'http://thompsonmachinerysales.com/wp-content/uploads/2017/02/IMG_07651.jpg'],
      'location': 'Winona',
      'make': 'unknown',
      'miles': 0,
      'model': 'unknown',
      'notes': 'Added GPS. New Blade',
      'owner': 'Jason',
      'personal_id': 'unknown',
      'serial': 'X035654X',
      'sticker_id': '',
      'user_email': 'jasonwoolliscroft@gmail.com',
      'vin': 'N/A',
      'year': 'unknown',
      'value': 112000
    },
  ],
  all: function(params, callback) {
    client.scan({TableName: tableName}, callback);
    //return this.equipment
  },
  get: function(params, callback) {
    callback = callback || function(err, data) { console.error('Missing callback!'); };
    db.getItem({
        TableName: tableName,
        Key: {
          'user': {'S': params.user || ' '},
          'create_date': {'S': params.date || ' '},
        }
      }, function(err, data) {
        let item = {

        };
        let i = data.Item;

        if (i.description && i.description.S) { item.description    = i.description.S; }
        if (i.gen_id      && i.gen_id.S)      { item.gen_id    = i.gen_id.S; }
        if (i.hours       && i.hours.N)       { item.hours  = i.hours.N; }
        if (i.miles       && i.miles.N)       { item.miles  = i.miles.N; }
        if (i.vin         && i.vin.S)         { item.vin    = i.vin.S; }
        if (i.serial      && i.serial.S)      { item.serial = i.serial.S; }
        if (i.model       && i.model.S)       { item.model  = i.model.S; }
        if (i.notes       && i.notes.S)       { item.notes  = i.notes.S; }
        if (i.value       && i.value.N)       { item.value  = i.value.N; }
        if (i.images      && i.images.SS)     { item.images  = {values: i.images.SS}; }

        callback(err, {
          model: i,
          simple: item
        });
      }
    );
    // const isSelected = p => p.id === id;
    // return this.equipment.find(isSelected)
  },
  save: function(data, callback) {

    let model = data.model;
    let item = data.simple;

    Object.keys(item).forEach(k => {
      let value = item[k];
      if (model.hasOwnProperty(k)) {
        const fieldType = Object.keys(model[k])[0];
        model[k][fieldType] = convertValueByFieldType(fieldType, value);
      } else {
        console.warn(`${k} does not exist in model!`);
        // attempt to set string field
          if (value !== '') {
            // is there a field type? if not use string
            let fieldType = fields[k] ? fields[k] : 'S';
            model[k] = {[fieldType]: convertValueByFieldType(fieldType, value)};
          }
      }
    });

    db.putItem({
        TableName: tableName,
        Item: model
    }, callback)

    // this.equipment = this.equipment.map(e => {
    //   return (e.id === item.id) ? item : e;
    // });
    // return true;
  }
}

export default EquipmentAPI

